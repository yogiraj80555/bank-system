/*
This is simple Bank System Which i have developed during me time appriciate if any suggestion
Thankyou
*/
#include<iostream>
#include<vector>
#include<string.h>

class Account{
    
    char name[100],address[100];
    char accountType;
    int accountNo;
    float ammount;
    public:
    Account(){}
    void createAccount();
    void creditMoney();
    void displayAccount();
    void debitMoney();
    int getaccountID();
    void setaccountID(int);
    float getBalance();
    friend Account* notFoundData(Account*);

};


float Account:: getBalance(){
    return ammount;
}

void Account::setaccountID(int a){
    accountNo = a;
}

int Account::getaccountID(){
    return accountNo;
}

void Account::createAccount(){
    std::cout<<"Enter Your Name: ";
    std::cin.ignore();
    std::cin.getline(name, 100);
    std::cout<<"Enter Your Address: ";
    std::cin.ignore();
    std::cin.getline(address,100);
    std::cout<<"what type of account want to Open ->\n1)Saving\n2)Current\n";
    int a;
    std::cin>>a;
    if(a == 1)
        accountType = 'S'; 
    else if(a == 2)
        accountType = 'C';
    else
    {
        accountType = 'N'; //None type of account
    }
    ammount = 0;

}

void Account::creditMoney(){
    float a;
    std::cout<<"Enter Creadited Money\n Should be less than $50000 in one time";
    std::cin>>a;
    if(a > 50000){
        a=50000;
        std::cout<<"\n Your Amount 50000 is accepted for an current deposite";
    }
    ammount += a;
    std::cout<<"Your amount "<<a<<" Creadited Successfully\n Your Current Bank Balance is: "<<ammount<<"\n Thankyou for Banking with us!"; 
}

void Account::displayAccount(){
    std::cout<<"Account Name: "<<name<<"\nAddress: "<<address<<"\nAccount Balance:"<<ammount;
    if(accountType == 'S')
    {
        std::cout<<"\nAccount Type: Saving";
    }
    else if(accountType == 'C'){
        std::cout<<"\nAccount Type: Current";
    }else{
        std::cout<<"\nAccount Type: Note Avalible";
    }
    std::cout<<"\n\n";
}

void Account::debitMoney(){
    int a;
    std::cout<<"You can debit maximum $50000 at a onr time\nEnter Intege value Debit balance: ";
    std::cin>>a;
    if(a > 50000){std::cout<<"We accepted $50000 to be debited form your balance\nPlease wait for transaction Completed"; a=50000;}
    if(a > ammount){
        std::cout<<"Sorry your transaction Could not be completed due to insufficiant balance";
    }else{
        ammount -= a;
        std::cout<<"Your transaction is completed Successfully\n Your Current Balance is "<<ammount<<"\nThankyou for Banking with us!";
    }

}



class Bank{
    
    std::vector<Account*> accounts;
    int account_id=0;
    Account* getAccount(int,int,int);
    Account* getAccountDetails(){
        std::cout<<"Enter Account No: ";
        short int a;
        std::cin>>a;
        Account* x;
        if (accounts.size() == 0){
            x = notFoundData(x);
        }else{
         x= getAccount(0,accounts.size(),a);
        }
        x->displayAccount();
        return x;
        
    }
    public:
    

    void accountList(){
        std::cout<<"Account List As Follow: \n";
        for(int i=0; i< accounts.size(); i++){
            std::cout<<"Account Id is: "<<accounts[i]->getaccountID()<<"\nAccount Balance: "<<accounts[i]->getBalance()<<"\n\n";
        }
    }

    void createAccount(){
        Account *a = new Account;
        a->createAccount();
        a->setaccountID(++account_id);
        accounts.push_back(a);

        std::cout<<"\nYour Account is been created details as below: \nAccount Number:"<<account_id<<std::endl;
        a->displayAccount();
        
    }

    void creditBalance(){
        Account* a = getAccountDetails();
        if(a[0].getaccountID() == 0){
            std::cout<<"Sorry We cant find your Account id\nPlease try again";
        }else{
            a->creditMoney();
            std::cout<<std::endl<<std::endl;
        }

        
    }

    void getBankBalance(){
        getAccountDetails();
    }

    void debitBalance(){
       Account* a =  getAccountDetails();
        if(a[0].getaccountID() == 0){
            std::cout<<"Sorry We cant find your Account id\nPlease try again";
        }else{
            a->debitMoney();
            std::cout<<std::endl<<std::endl;
        }

    }

    void bankMoney(){
        if(accounts.size() == 0){
            std::cout<<"Bank Has No Money";
        }
        float x = 0;
        for(int i=0; i<accounts.size(); i++){
            x += accounts[i]->getBalance();
        }

        std::cout<<"Total Bank has $"<<x<<std::endl;
    }
};


Account* Bank:: getAccount(int first,int last,int key){
    /*
        In this function we used binary search to get account 
    */

    int mid = (first+last)/2;
    if(accounts[mid]->getaccountID() == key){return accounts[mid];}
    else if(first==last){ 
        Account* a = new Account; 
        return notFoundData(a); }
    else if(accounts[mid]->getaccountID() < key){  return getAccount(mid+1,last,key);  }
    //else if(accounts[mid].getaccountID() > key){  return getAccount(first,mid-1,key); }
    else {  return getAccount(first,mid-1,key); }
    
}

Account* notFoundData(Account* c){
    strcpy(c[0].name,"Not Found");
    strcpy(c[0].address,"Not Avalible");
    c[0].ammount = 0.00;
    c[0].accountType = 'N';
    c[0].accountNo = 0;
    return c; 
}

int main(){
    int a ;
    system("cls");
    std::cout<<"Welcome to banking Service";
    Bank b;
    
    while(true){
        std::cout<<"\n\n1)Create Account"<<std::endl<<"2)Crdit Money\n3)Display Balance with Details\n4)Debit Money\n5)Display All Accounts\n6)Total Bank Money\nPress any other key for Exit";
        std::cin>>a;
        if(a==1)
            b.createAccount();
        else if(a==2)
            b.creditBalance();
        else if(a==3)
            b.getBankBalance();
        else if(a==4)
            b.debitBalance();
        else if(a==5)
            b.accountList();
        else if(a==6)
            b.bankMoney();
        else
        {
            break;
        }

        
        
            
    }
    
    std::cout<<"Thankyou for Banking with us!\n Welcome to come again\nHave a nice day!\n\n";

    return 0;
}